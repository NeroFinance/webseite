# Webseite von Meryem 

## Vorteile der Webseite

- Aufbau über Jekyll


### Voraussetzungen

- ruby größer 3.x, oder im .ruby-version betrachten
- wenn chruby + autoLoad installiert ist wird es automatisch die ruby version nehmen die im .ruby-version eingetragen ist, umschalten

### VScode hat recommended plugins

- Bitte im extension @recommended aufrufen und die benötigten extension installieren (EditorConfig for VS Code, Ruby language support, Shopify Liquid)
- für das EditorConfig wird auch das passende Modul gebraucht das erreichen wir mit   vorausgesetzt nodejs ist installiert

    ```bash
    npm install
    ```

### Erstbetrieb

- bundle install


### start für lokale Umgebung (oder Github Pages)

```bash
bundle exec jekyll serve --livereload
```

## Lokal auf dem browser und nach dem push

- [Server address] (auf port 4000)


## Freigabe des Portes 

- checken ob der Port frei ist 
```bash
lsof -i :4000
```

- Freigabe des Portes 
```bash
kill -9 [PID]
```

## Authors and acknowledgment
 Melih Omay
 Keihan Pakseresht

